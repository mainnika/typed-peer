import * as express from "express";
import * as http from "http";
import * as https from "https";

export interface PeerServerOptions {
	debug?: boolean,
	timeout?: number,
	key?: string,
	ip_limit?: number,
	concurrent_limit?: number,
	allow_discovery?: boolean,
	proxied?: boolean,
}

export declare class ExpressPeerServer {
	constructor(server: express.Application, options: PeerServerOptions);
}

export declare class PeerServer {
	constructor(options: PeerServerOptions, callback: (server: http.Server|https.Server) => void);
}
